import re

file = input("enter the file name: ")
mlist = {}
total = 0
count = int(0)
comp = open(file)
expression = input("Enter a regular expression: ")
try:
    for line in comp:
        line = line.rstrip()
        if re.search(expression, line):
            mlist = re.findall('[0-9]+', line)
            total += int(mlist[0])
            count += 1

    print(total/count)
except FileNotFoundError:
    print("Can't find the file", file)
