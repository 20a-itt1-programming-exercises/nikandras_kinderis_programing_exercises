file = input("enter a filename")
averagenumber = 0
count = 0
if file == "na na boo boo":
    print("NA NA BOO BOO TO YOU - You have been punk'd!")
else:
    with open(file) as infile:
        for line in infile:
            if line.startswith('X-DSPAM-Confidence:'):
                targetindex = line.find(" ")
                founddata = float(line[targetindex + 1:])
                averagenumber += founddata
                count += 1
        print("Average spam confidence:", averagenumber / count)