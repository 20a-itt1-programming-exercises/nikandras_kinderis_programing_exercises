sample = open("sample.txt")
calendar = dict()
for line in sample:
    line = line.rstrip()
    if not line.startswith("From"):
        continue
    words = line.split()
    if len(words) == 0:
        continue
    if len(words) < 3:
        continue
    day = words[2]
    if day not in calendar:
        calendar[day] = 1
    else:
        calendar[day] += 1

print(calendar)
