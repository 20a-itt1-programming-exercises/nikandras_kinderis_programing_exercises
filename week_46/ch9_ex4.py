sample = open(input("Enter a file name: "))
addresses = dict()
maximum = None
count = 0
for line in sample:
    line = line.rstrip()
    if not line.startswith("From"):
        continue
    words = line.split()
    if len(words) == 0:
        continue
    if "@" not in words[1]:
        continue
    email=words[1]
    if email not in addresses:
        addresses[email] = 1
        count += 1
    else :
        addresses[email] += 1
        count += 1

for itervar in addresses:
    if maximum is None or count > maximum:
        maximum = count

print (email,maximum)