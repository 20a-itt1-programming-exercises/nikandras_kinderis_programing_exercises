sample = open("sample.txt")
addresses = dict()
for line in sample:
    line = line.rstrip()
    if not line.startswith("From"):
        continue
    words = line.split()
    if len(words) == 0:
        continue
    if "@" not in words[1]:
        continue
    email=words[1]
    if email not in addresses:
        addresses[email] = 1
    else :
        addresses[email] += 1