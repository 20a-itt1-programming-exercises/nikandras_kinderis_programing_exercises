sample = open(input("Enter a file name: "))
domain = dict()
for line in sample:
    line = line.rstrip()
    if not line.startswith("From"):
        continue
    words = line.split()
    if len(words) == 0:
        continue
    if "@" not in words[1]:
        continue
    email=words[1]
    if email not in domain:
        domain[email] = 1
    else :
        domain[email] += 1

mails=list(domain.keys())
for key in mails:
    key = key.split("@")
    print(key[1])