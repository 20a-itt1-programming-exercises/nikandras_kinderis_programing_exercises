file = input("enter a file name: ")
hours_histogram = dict()
largest = None
max_receiver = ''

for line in file:
    if line.startswith('From '):
        words = line.split()
        hour = words[5][:2]
        if hour not in hours_histogram:
            hours_histogram[hour] = 1
        else:
            hours_histogram[hour] += 1

mlist = list()

for hour, count in list(hours_histogram.items()):
    mlist.append((hour, count))

mlist.sort()

for item in mlist:
    print(*item)
