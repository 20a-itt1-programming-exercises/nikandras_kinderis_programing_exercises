notgood = ['\n', ' ', '-', '.', ',', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '!', '\\', '*', '_', '{', '}', ' [ ', ']', '(', ')', '>', '#', ' +', '$', '\'']

file =input("enter a file name: ")

letterHistogram = dict()

for line in file:
    if line != '':
        for char in notgood:
            line = line.replace(char, "")
        line = line.lower()

        for letter in line:
            if letter not in letterHistogram:
                letterHistogram[letter] = 1
            else:
                letterHistogram[letter] += 1
        else:
            continue

mlist = list()

for letter, count in list(letterHistogram.items()):
    mlist.append((count, letter))

mlist.sort()
