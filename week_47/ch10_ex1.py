try:

    file = input("enter the file name: ")
    mails_histogram = dict()
    largest = None
    max_receiver = ''

    for line in file:
        if line.startswith('From: '):
            address = line[line.find(' ') + 1:].rstrip()
            if address not in mails_histogram:
                mails_histogram[address] = 1
            else:
                mails_histogram[address] += 1

    mlist = list()

    for name, count in list(mails_histogram.items()):
        mlist.append((name, count))

    mlist.sort(reverse=True)

    print(*mlist[0])

except FileNotFoundError:
    print('File not found')
