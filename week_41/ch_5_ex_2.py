minimum = None
maximum = None
while True:
    try:
        number = input("enter a number")

        if number == "done":
            print("maximum:", maximum)
            print ("minimum", minimum)
            break
        else:
            number = float(number)

        if maximum is None or number > maximum:
            maximum = number
        if minimum is None or number<minimum:
            minimum = number

    except:
        print("bad input")