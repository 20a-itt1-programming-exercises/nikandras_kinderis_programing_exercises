from person import Extended_Person, Person
from datetime import datetime


person1 = Person("Anders Andersen", datetime(1901, 1, 1), ("Avej", "1", "Astrup", "1111", "DK"), "male")
person2 = Person("Bente bent", datetime(2001, 2, 12), ("Byvej", "2", "Bullerup", "2222", "DK"), "male")
person3 = Person("Catja Caj", datetime(2001, 3, 23), ("Cykelvej", "3", "Castrup", "3333", "DK"), "male")

print(person1.get_full_info())
print(person2.get_full_info())
print(person3.get_full_info())


person1 = Extended_Person("Anders Andersen", datetime(1901, 1, 1), ("Avej", "1", "Astrup", "1111", "DK"), "male", "dog", "doggy", "42069")
person2 = Extended_Person("Bente bent", datetime(2001, 2, 12), ("Byvej", "2", "Bullerup", "2222", "DK"), "male", "llama", "Dali llama", "20")
person3 = Extended_Person("Catja Caj", datetime(2001, 3, 23), ("Cykelvej", "3", "Castrup", "3333", "DK"), "male", "Albino death-bullfrog", "casper", "99999999")

print(person1.get_pet(), person1.get_income())
print(person2.get_pet(), person2.get_income())
print(person3.get_pet(), person3.get_income())

