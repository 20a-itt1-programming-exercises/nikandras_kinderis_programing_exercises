import socket

try:
    mysock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    url = input("enter an URL: ")
    web = url.split("/")[2]
    mysock.connect((web, 80))
    cmd = f'GET {web} HTTP/1.0\r\n\r\n'.encode()
    mysock.send(cmd)

except:
    print("the URl is writen badly ")

while True:
    data = mysock.recv(512)
    if len(data) < 1:
        break
    print(data.decode(), end='')

mysock.close()
