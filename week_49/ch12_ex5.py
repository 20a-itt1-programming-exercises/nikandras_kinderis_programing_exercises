import socket

try:
    mysock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    url = input("enter an URL: ")
    web = url.split("/")[2]
    mysock.connect((web, 80))
    cmd = f'GET {web} HTTP/1.0\r\n\r\n'.encode()
    mysock.send(cmd)
except:
    print("the URl is writen badly ")
count = 0

data = mysock.recv(512)
message = data.decode()
header_end_pos = message.find('\r\n\r\n')  # adds 4 to exclude \r\n\r\n and make sure it starts from the right place
print(message[header_end_pos:])

while True:
    data = mysock.recv(512)
    if not data:
        break
    count += len(data)
    print(data.decode())


print("The number of characters in the site: ", count)
mysock.close()
