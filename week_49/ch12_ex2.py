import socket

try:
    mysock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    url = input("enter an URL: ")
    web = url.split("/")[2]
    mysock.connect((web, 80))
    cmd = f'GET {web} HTTP/1.0\r\n\r\n'.encode()
    mysock.send(cmd)

except:
    print("the URl is writen badly ")
count = 0
while True:
    data = mysock.recv(512)
    if len(data) < 1:
        break
    count += len(data)
    print(data.decode(), end='')

print("The number of characters in the site: ", count)
mysock.close()
